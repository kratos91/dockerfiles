# Comando en Docker compose

-----------------------------------------------------------
## COMANDOS PARA IMAGENES EM COMPOSE
-----------------------------------------------------------
### Crear imagen
```sh
$ docker-compose build
```

-----------------------------------------------------------
## COMANDOS PARA CONTENEDORES COMPOSE
-----------------------------------------------------------
### Crear contenedor
```sh
$ docker-compose up -d
```

### Eliminar contenedor
```sh
$ docker-compose down
```