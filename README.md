# Repositorio de Dockerfiles

----------------------------------------------------------
## COMANDOS PARA CREAR
----------------------------------------------------------
### Construir imagen
```sh
$ docker build -t nombre_imagen:version .
```

### Construir imagen con un dockerfile de diferente nombre
```sh
$ docker build -t nombre_imagen:version -f my-dockerfile .
```
### Crear contenedores
```sh
$ docker run -d --name nombre -p 8080:80 nombre_imagen:version
```

### Crear contenedores con limites de memoria RAM y cpus
```sh
$ docker run -d -m "500mb" --cpuset-cpus 0-1 --name nombre_contenedor -p 2201:27017 nombre_imagen
```

----------------------------------------------------------
## COMANDOS PARA LEER
-----------------------------------------------------------
### Revisar contenedores
```sh
$ docker ps -a
```

### Revisar imagenes
```sh
$ docker images
```

### Revisar imagenes danglings
```sh
$ docker images -f dangling=true
```

-----------------------------------------------------------
## COMANDOS PARA ACTUALIZAR
-----------------------------------------------------------

### Cambiar nombre a un contenedor
```sh
$ docker rename nombre_actual nuevo_nombre
```

-----------------------------------------------------------
## COMANDOS PARA ELIMINAR
-----------------------------------------------------------
### Eliminar imagen
```sh
$ docker rmi id_imagen || nombre_imagen:tag
```

### Eliminar imagenes dangling
```sh
$ docker images -f dangling=true -q | xargs docker rmi
```

### Eliminar contenedor
```sh
$ docker rm -f nombre_imagen
```


-----------------------------------------------------------
## COMANDOS PARA DETENER/REINICIAR
-----------------------------------------------------------

### Comando para detener
```sh
$ docker stop id_contenedor/nombre_contenedor
```


### Comando para iniciar
```sh
$ docker start id_contenedor/nombre_contenedor
```

### Comando para reiniciar
```sh
$ docker restart id_contenedor/nombre_contenedor
```

-----------------------------------------------------------
## OTROS COMANDOS
-----------------------------------------------------------
### Comando para ingresar a la terminal del contenedor
```sh
$ docker exec -u root -ti nombre_contenedor bash
```

### Comando para ver los recursos ocupados por un contenedor
```sh
$ docker stats mi_contenedor
```

### Comando para copiar archivos a un contenedor y vicerversa
```sh
$ docker cp nombre_archivo nombre_contenedor:/ruta_del contenedor
$ docker cp nombre_contenedor:/ruta_del_archivo /ruta_computador
```


### Certificado ssl
```sh
openssl req -x509 -nodes -days 365 -newkey rsa:2048 -keyout mysitename.key -out mysitename.crt
```

```
